package system_info;

import org.jutils.jhardware.HardwareInfo;
import org.jutils.jhardware.model.GraphicsCard;
import org.jutils.jhardware.model.MemoryInfo;
import org.jutils.jhardware.model.OSInfo;
import org.jutils.jhardware.model.ProcessorInfo;

import java.io.Serializable;

public class SystemInfo implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String osFullName;
	private String memoryInfo;
	private String processorInfo;
	private String videoCardInfo;
	
	public SystemInfo() {
	}
	
	@Override
	public String toString() {
		return	"OS: " + osFullName + "\n"
			+	"Memory: " + memoryInfo + "\n"
			+	"Processor: " + processorInfo + "\n"
			+	"Videocard: " + videoCardInfo + "\n";
	}
	
	public static SystemInfo create() {
		SystemInfo info = new SystemInfo();
		
		OSInfo os = HardwareInfo.getOSInfo();
		info.osFullName = os.getManufacturer() + " / " + os.getName();
		if ( os.getVersion() != null )
			info.osFullName = info.getOsFullName() + " / " + os.getVersion();

		MemoryInfo mem = HardwareInfo.getMemoryInfo();
		info.memoryInfo = "Available: " +  mem.getAvailableMemory() + " / Free: " + mem.getFreeMemory();

		ProcessorInfo proc = HardwareInfo.getProcessorInfo();
		info.setProcessorInfo(proc.getModelName() + " " +  proc.getModel() + " / " + proc.getMhz());

		for( GraphicsCard card : HardwareInfo.getGraphicsCardInfo().getGraphicsCards() ) {
			info.setVideoCardInfo(card.getManufacturer() + " " + card.getName() + ";");
		}
		
		return info;
	}

	public String getOsFullName() {
		return osFullName;
	}

	public void setOsFullName(String osFullName) {
		this.osFullName = osFullName;
	}

	public String getMemoryInfo() {
		return memoryInfo;
	}

	public void setMemoryInfo(String memoryInfo) {
		this.memoryInfo = memoryInfo;
	}

	public String getProcessorInfo() {
		return processorInfo;
	}

	public void setProcessorInfo(String processorInfo) {
		this.processorInfo = processorInfo;
	}

	public String getVideoCardInfo() {
		return videoCardInfo;
	}

	public void setVideoCardInfo(String videoCardInfo) {
		this.videoCardInfo = videoCardInfo;
	}

}
