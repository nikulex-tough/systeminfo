package system_info;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

public class ServerConfig {
	private static final String CONFIG_FILE = "server.config";
	
	private static final String DB_FILE = "DatabaseFile";
	private String dbFile;

	private ServerConfig() {
	}
	
	private void setDefault() {
		dbFile = "system_info.db";
	}
	
	public String getDBFileName() {
		return dbFile;
	}
	
	public static ServerConfig load() throws FileNotFoundException, IOException {
		ServerConfig config = new ServerConfig();
		Properties properties = new Properties();
		
		try {
			properties.load( new FileInputStream( CONFIG_FILE ) );
			
			config.dbFile = properties.getProperty( DB_FILE );
		} catch ( Exception ex ) {
			config.setDefault();
			
			properties.setProperty( DB_FILE, config.dbFile );
			
			properties.save( new FileOutputStream( CONFIG_FILE ), "" );
		}
		
		return config;
	}
}
