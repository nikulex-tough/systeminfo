package system_info;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class ClientConfig {

	private static final String CONFIG_FILE = "client.config";
	
	private static final String SERVER_ADDRESS = "ServerAddress";
	private String serverAddress;
	
	private ClientConfig() {
	}
	
	public String getServerAddress() {
		return serverAddress;
	}
	
	private void setDefault() {
		serverAddress = "localhost";
	}
	
	public static ClientConfig load() throws FileNotFoundException, IOException {
		ClientConfig config = new ClientConfig();
		Properties properties = new Properties();
		
		try {
			properties.load( new FileInputStream( CONFIG_FILE ) );
			
			config.serverAddress = properties.getProperty( SERVER_ADDRESS );
			
		} catch ( Exception ex ) {
			config.setDefault();
			
			properties.setProperty( SERVER_ADDRESS, config.serverAddress );
			
			properties.save( new FileOutputStream( CONFIG_FILE ), "" );
		}
		
		return config;
	}
}
