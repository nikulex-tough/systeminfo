package system_info;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

import com.google.gson.Gson;

public class Server {
    public static void main(String[] args) throws Exception {

    	ServerConfig config = ServerConfig.load();
    	
        System.out.println("The server is running.");
        int clientNumber = 1;
        ServerSocket listener = new ServerSocket(Global.PORT);
        try {
            while (true) {
                new ClientListener(listener.accept(), config, clientNumber++).start();
            }
        } finally {
            listener.close();
        }
    }

    private static class ClientListener extends Thread {
        private Socket socket;
        private ServerConfig config;
        private int id;
        private BufferedReader in;

        public ClientListener(Socket socket, ServerConfig config, int id) throws IOException {
            this.socket = socket;
            this.config = config;
            this.id = id;
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
            log("New connection with client #" + id + " at " + socket);
        }
        
        private void writeToDb( SystemInfo info ) throws IOException {
        	SystemInfoRepository repository = null;
        	try {
				repository = new SystemInfoRepository( config.getDBFileName() );
				repository.addSystemInfo( socket.getRemoteSocketAddress().toString(), info );
            	log( "Write info client #" + id + " " + socket.getRemoteSocketAddress().toString() );
			} catch (ClassNotFoundException | SQLException e) {
				log( "Error: " + e.getMessage() );
			} finally {
				repository.close();
			}
        }

        public void run() {
            try {
            	String serializedObject = in.readLine();
                if (serializedObject != null && !serializedObject.equals(".")) {
                    SystemInfo info = new Gson().fromJson(serializedObject, SystemInfo.class);
                    writeToDb( info );
                    
                } else {
                	log( "Load error with client #" + id );
                }
            } catch (IOException e) {
            	log("Error handling client #" + id + ": " + e);
                
            } finally {
            	try {
                    socket.close();
                } catch (IOException e) {
                    log("Couldn't close a socket, what's going on?");
                }
                
                log("Connection with client #" + id + " closed");
            }
        }

        private void log(String message) {
            System.out.println(message);
        }
    }
}