package system_info;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import com.google.gson.Gson;

public class Client {
    public static void main(String[] args) throws IOException, InterruptedException {
    	ClientConfig config = ClientConfig.load();

    	System.out.println( "Loading info..." );
    	
    	Socket socket = null;
        try {
        	SystemInfo info = SystemInfo.create();
        	System.out.println( info.toString() );

        	System.out.println( "Sending info..." );
        	
        	socket = new Socket(config.getServerAddress(), Global.PORT);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
        	out.println( new Gson().toJson( info ) );      

        	System.out.println( "Information sent successfully!" );
        	
        } catch ( SocketException ex ) {
        	System.out.println( "Connection error: " + ex.getMessage() );      

        } catch ( Exception ex ) {
        	System.out.println( "Error: " + ex.getMessage() );  
        	
        } finally {
        	socket.close();
        }
    }
}