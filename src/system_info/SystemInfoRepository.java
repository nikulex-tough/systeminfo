package system_info;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class SystemInfoRepository implements Closeable {
	
	private Connection connection;

	public SystemInfoRepository( String dbPath ) throws ClassNotFoundException, SQLException {
    	try {
    	      Class.forName("org.sqlite.JDBC");
    	      connection = DriverManager.getConnection( "jdbc:sqlite:" + dbPath );
    	      createIfNotExistTables();
    	} finally {
    	}
	}
	
	private void createIfNotExistTables() {
		final String sql
			=	"CREATE TABLE IF NOT EXISTS SystemInfo ("
			+		"id INTEGER PRIMARY KEY AUTOINCREMENT,"
			+		"ip VARCHAR(16),"
			+		"log_date DATETIME DEFAULT CURRENT_TIMESTAMP,"
			+		"os_full_name VARCHAR,"
			+		"memory_info TEXT,"
			+		"processor_info TEXT,"
			+		"videocard_info TEXT"
			+	");";

		try {
			Statement statement = connection.createStatement();
			statement.execute( sql );
		} catch (SQLException e) {
			System.out.println( "Create tables error: " + e.getMessage() );
		}
	}
	
	
    public void addSystemInfo( String ip, SystemInfo info ) {
    	final String sql
    	=	"INSERT INTO SystemInfo( ip, os_full_name, memory_info, processor_info, videocard_info) VALUES("
    	+	"\'" + ip.toString() + "\',"
    	+	"\'" + info.getOsFullName() + "\',"
    	+	"\'" + info.getMemoryInfo() + "\',"
    	+	"\'" + info.getProcessorInfo() + "\',"
    	+	"\'" + info.getVideoCardInfo() + "\');";

		try {
			Statement statement = connection.createStatement();
			statement.executeUpdate( sql );
		} catch (SQLException e) {
			System.out.println( "Insert error: " + e.getMessage() );
		}
    }
    
    /*
    private void transactionWrapper( Runnable operation ) {
    	try {
    		operation.run();
    		connection.commit();
    	} catch ( SQLException ex ) {
    		System.out.println( "SQL Error: " + ex.getMessage() );
    		
    		try {
				connection.rollback();
			} catch (SQLException ex2) {
	    		System.out.println( "SQL Error: " + ex2.getMessage() );
			}
    	}
    }
    */

	@Override
	public void close() throws IOException {
		try {
			if ( connection != null && !connection.isClosed() )
				connection.close();
		} catch (SQLException e) {
		}
	}
}
